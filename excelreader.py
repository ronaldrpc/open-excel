import openpyxl
import re
import pprint


def is_valid_email(email):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    return True if re.fullmatch(regex, email) else False


class ExcelReader:
    def __init__(self, path):
        self.path = path

    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            print(sheet_obj.cell(1, col + 1).value)
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)

        return data

    def clean_data(self, data):
        for items in data:
            print(is_valid_email(items.get('Correo')))

        return self

    def test_email(self, email):
        return is_valid_email(email)


def main():
    reader = ExcelReader('')
    print(reader.test_email("kekw"))
    print(reader.test_email("r@f.com"))


if __name__ == "__main__":
    main()
